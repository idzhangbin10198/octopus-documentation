---
title: "Octopus Docker images"
---

Here we assume that you already have docker on your system or know how to install it. In case, you need support, see the official documentation on [installing docker](https://docs.docker.com/engine/install/)

Once you have a running version of docker on your machine, you can download a docker image, containing a working installation of {{<octopus>}} with the command:
```bash
$ docker pull fangohr/octopus:12.1
```
You can directly check the image by running
```bash
$ docker run --rm fangohr/octopus:12.1 octopus --version
```
If the octopus version is displayed, all should be well.


If you want to explore more, you can jump into the container using
```bash
$ docker run --rm -ti -v $PWD:/io fangohr/octopus:12.1 bash
```
There are some trivial simulation configurations available in /opt/octopus-examples.

To run your own input files, which you have prepared on your computer, 
you need to tell docker to ‘mount’ your current directory into the container. 
In the folder, where you want to run {{<octopus>}}, use the command:
```bash
$ docker run --rm -ti -v $PWD:/io fangohr/octopus:12.1 octopus
```
This should behave as if you had installed {{<octopus>}} on your system.
