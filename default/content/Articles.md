---
title: "Publications using Octopus"
weight: 20 
---


''If you have published an article where Octopus was used, feel free to add it here (or if you prefer send us the link and we will do it)''.

You can also check the [Octopus researcher ID page](http://www.researcherid.com/rid/E-2400-2011) for further publications.

## 2023

- {{<article
title="Constructing semilocal approximations for noncollinear spin density functional theory featuring exchange-correlation torques"
authors="N. Tancogne-Dejean, A. Rubio, and C. A. Ullrich"
journal="Phys. Rev. B"
volume="107"
pages="165111"
year="2023"
doi="10.1103/PhysRevB.107.165111"
>}}

- {{<article
title="Attosecond magnetization dynamics in non-magnetic materials driven by intense femtosecond lasers"
authors="O. Neufeld, N. Tancogne-Dejean, U. De Giovannini, H. Hübener, and A. Rubio"
journal="npj Computational Materials"
volume="9"
pages="39"
years="2023"
doi="10.1038/s41524-023-00997-7"
>}}


## 2022

- {{<article
title="Impact of electron correlation on the light-induced demagnetization of elemental ferromagnetic metals"
authors="T. Barros, N. Tancogne-Dejean, J. Berakdar, and M. A. L. Marques"
journal="Eur. Phys. J. B"
volume="95"
pages="175"
year="2022"
doi="10.1140/epjb/s10051-022-00433-76"
>}}

- {{<article
title="Polarization spectroscopy of high-order harmonic generation in gallium arsenide"
authors="S. Kaasamani, T. Auguste, N. Tancogne-Dejean, et al."
journal="Optics express"
volume="30"
pages="40531"
years="2022"
doi="10.1364/OE.468226"
>}}

- {{<article
title="Time- and angle-resolved photoelectron spectroscopy of strong-field light-dressed solids: Prevalence of the adiabatic band picture"
authors="O. Neufeld, W. Mao, H. Hübener, N. Tancogne-Dejean, et al."
journal="Phys. Rev. Research"
volume="4"
pages="033101"
year="2022"
doi="10.1103/PhysRevResearch.4.033101"
>}}

- {{<article
title="Effect of spin-orbit coupling on the high harmonics from the topological Dirac semimetal Na3Bi"
authors="N. Tancogne-Dejean, F. G. Eich, and A. Rubio"
journal="npj Computational Materials"
volume="8"
pages="145"
year="2022"
doi="10.1038/s41524-022-00831-6"
>}}

- {{<article
title="Ab Initio Cluster Approach for High Harmonic Generation in Liquids"
authors="O. Neufeld, Z. Nourbakhsh, N. Tancogne-Dejean, and A. Rubio"
journal="J. Chem. Theory Comput."
volume="18"
pages="4117"
year="2022"
doi="10.1021/acs.jctc.2c00235"
>}}

- {{<article
title="Filming Movies of Attosecond Charge Migration in Single Molecules with High Harmonic Spectroscopy"
authors="L. He, S. Sun, P. Lan, Y. He, B. Wang, P. Wang, X. Zhu, L. Li, W. Cao, P. Lu, and C. D. Lin"
journal="Nat. Commun." 
volume="13"
pages="4595" 
year="2022"
doi="10.1038/s41467-022-32313-0"
>}}


## 2021 

- {{<article
title="Photoionization and transient Wannier-Stark ladder in silicon: First-principles simulations versus Keldysh theory"
authors="T. J.-Y. Derrien, N. Tancogne-Dejean, V. P. Zhukov, H. Appel, A. Rubio, and N. M. Bulgakova"
journal="Physical Review B"
volume="104"
pages="L241201"
year="2021"
doi="10.1103/PhysRevB.104.L241201"
>}}

- {{<article
title="Light-Driven Extremely Nonlinear Bulk Photogalvanic Currents"
authors="O. Neufeld, N. Tancogne-Dejean, U. De Giovannini, H. Hübener, and A. Rubio"
journal="Physical Review Letters"
volume="127"
pages="126601"
year="2021"
doi="10.1103/PhysRevLett.127.126601"
>}}

- {{<article
title="Ultrafast dynamical Lifshitz transition"
authors="Beaulieu, Samuel and Dong, Shuo and Tancogne-Dejean, Nicolas and others"
journal="Science Advances"
volume="7"
pages="eabd9275"
year="2021"
doi="10.1126/sciadv.abd9275" 
>}}

- {{< article 
        title="High Harmonics and Isolated Attosecond Pulses from MgO" 
        authors="Z. Nourbakhsh, N. Tancogne-Dejean, H. Merdji, and A. Rubio" 
        journal="Phys. Rev. Applied" 
        volume="15" 
        pages="014013" 
        year="2021" 
        doi="10.1103/PhysRevApplied.15.014013" 
    >}}
## 2020 
- {{< article 
        title="Parameter-free hybridlike functional based on an extended Hubbard model: DFT+U+V" 
        authors="N. Tancogne-Dejean, and A. Rubio" 
        journal="Physical Review B" 
        volume="102" 
        pages="155117" 
        year="2020" 
        doi="10.1103/PhysRevB.102.155117" 
    >}}
- {{< article 
        title="Ultrafast transient absorption spectroscopy of the charge-transfer insulator NiO: Beyond the dynamical Franz-Keldysh effect" 
        authors="N. Tancogne-Dejean, M. A. Sentef, and A. Rubio" 
        journal="Physical Review B" 
        volume="102" 
        pages="115106" 
        year="2020" 
        doi="10.1103/PhysRevB.102.115106" 
    >}}
- {{< article 
        title="Light-Induced Renormalization of the Dirac Quasiparticles in the Nodal-Line Semimetal ZrSiSe" 
        authors="G. Gatti, A. Crepaldi, M. Puppin, N. Tancogne-Dejean, L. Xian, U. De Giovannini, S. Roth, S. Polishchuk, Ph. Bugnon, A. Magrez, H. Berger, F. Frassetto, L. Poletto, L. Moreschini, S. Moser, A. Bostwick, Eli Rotenberg, A. Rubio, M. Chergui, and M. Grioni" 
        journal="Physical Review Letters" 
        volume="125" 
        pages="076401" 
        year="2020" 
        doi="10.1103/PhysRevLett.125.076401" 
    >}}
- {{< article 
        title="Photomolecular High-Temperature Superconductivity" 
        authors="M. Buzzi, D. Nicoletti, M. Fechner, N. Tancogne-Dejean, M. A. Sentef, A. Georges, T. Biesner, E. Uykur, M. Dressel, A. Henderson, T. Siegrist, J. A. Schlueter, K. Miyagawa, K. Kanoda, M.-S. Nam, A. Ardavan, J. Coulthard, J. Tindall, F. Schlawin, D. Jaksch, and A. Cavalleri" 
        journal="Physical Review X" 
        volume="10" 
        pages="031028" 
        year="2020" 
        doi="10.1103/PhysRevX.10.031028" 
    >}}
- {{< article 
        title="Octopus, a computational framework for exploring light-driven phenomena and quantum dynamics in extended and finite systems" 
        authors="N. Tancogne-Dejean, M. J. T. Oliveira, X. Andrade, H. Appel, C. H. Borca, G. Le Breton, F. Buchholz, A. Castro, S. Corni, A. A. Correa, U. De Giovannini, A. Delgado, F. G. Eich, J. Flick, G. Gil, A. Gomez, N. Helbig, H. Hübener, R. Jestädt, J. Jornet-Somoza, A. H. Larsen, I. V. Lebedeva, M. Lüders, M. A. L. Marques, S. T. Ohlmann, S. Pipolo, M. Rampp, C. A. Rozzi, D. A. Strubbe, S. A. Sato, C. Schäfer, I. Theophilou, A. Welden, A. Rubio" 
        journal="The Journal of Chemical Physics" 
        year="2020" 
        volume="152" 
        pages="124119" 
        doi="10.1063/1.5142502" 
    >}}
- {{< article 
        title="Time-Dependent Magnons from First Principles" 
        authors="Nicolas Tancogne-Dejean, Florian G.Eich, Angel Rubio" 
        journal="Journal of Chemical Theory and Computation" 
        volume="16" 
        pages="1007" 
        year="2020" 
        doi="10.1021/acs.jctc.9b01064" 
    >}}
- {{< article 
        title="High-harmonic generation from spin-polarized defects in solids" 
        authors="M. S. Mrudul, N. Tancogne-Dejean, A. Rubio, G. Dixit" 
        journal="NPJ Computation Materials" 
        volume="6" 
        pages="10" 
        year="2020" 
        doi="10.1038/s41524-020-0275-z" 
    >}}

## 2019 
- {{< article 
        title="Deep Learning and Density Functional Theory" 
        authors="Kevin Ryczko, David A. Strubbe, Isaac Tamblyn" 
        journal="Physical Review A" 
        volume="100" 
        pages="022512" 
        year="2019" 
        url="https://journals.aps.org/pra/abstract/10.1103/PhysRevA.100.022512" 
    >}}
- {{< article 
        title="Multiflat Bands and Strong Correlations in Twisted Bilayer Boron Nitride: Doping-Induced Correlated Insulator and Superconductor" 
        authors="Lede Xian, Dante M. Kennes, Nicolas Tancogne-Dejean, Massimo Altarelli, Angel Rubio" 
        journal="Nano Letters" 
        year="2019" 
        doi="10.1021/acs.nanolett.9b00986" >}}
- {{< article 
        title="Polarization-state-resolved high-harmonic spectroscopy of solids" 
        authors="N. Klemke, N. Tancogne-Dejean, G. M. Rossi, Y. Yang, F. Scheiba, R. E. Mainz, G. Di Sciacca, A. Rubio, F. X. Kärtner, O. D. Mücke " 
        journal="Nature Communications" 
        volume="10" 
        pages="1319" 
        year="2019" 
        doi="10.1038/s41467-019-09328-1" 
    >}}

## 2018 
- {{< article 
        title="From a quantum-electrodynamical light--matter description to novel spectroscopies" 
        authors="Ruggenthaler, Michael and Tancogne-Dejean, Nicolas and Flick, Johannes and Appel, Heiko and Rubio, Angel" 
        journal="Nature Reviews Chemistry" 
        volume="2" 
        number="3" 
        pages="0118" 
        year="2018" 
    >}}
- {{< article 
        title="High-harmonic generation from few layer hexagonal boron nitride: Evolution from monolayer to bulk response" 
        authors="Guillaume Le Breton, Angel Rubio, and Nicolas Tancogne-Dejean" 
        journal="Phys. Rev. B" 
        year="2018" 
        volume="98" 
        pages="165308" 
        doi="10.1103/PhysRevB.98.165308" 
    >}}
- {{< article 
        title="All-optical nonequilibrium pathway to stabilising magnetic Weyl semimetals in pyrochlore iridates" 
        authors="G. Topp, N. Tancogne-Dejean, A. F. Kemper, A. Rubio, and M. A. Sentef" 
        journal="Nat. Commun." 
        year="2018" 
        volume="9" 
        pages="4452" 
        doi="10.1038/s41467-018-06991-8" 
    >}}
- {{< article 
        title="Ultrafast Modification of Hubbard U in a Strongly Correlated Material: Ab initio High-Harmonic Generation in NiO" 
        authors="Nicolas Tancogne-Dejean, Michael A. Sentef, and Angel Rubio" 
        journal="Phys. Rev. Lett." 
        year="2018" 
        volume="121" 
        pages="097402" 
        doi="10.1103/PhysRevLett.121.097402" 
    >}}
- {{< article 
        title="Atomic-like high-harmonic generation from two-dimensional materials" 
        authors="Nicolas Tancogne-Dejean, and Angel Rubio" 
        journal="Sci. Adv." 
        year="2018" 
        volume="4" 
        pages="eaao5207" 
        doi="10.1126/sciadv.aao5207" 
    >}}
- {{< article 
        title="Field Electron Emission Images Far Away from A Semi-infinitely Long Emitter: A Multi-scale Simulation" 
        authors="Weiliang Wang, Weitao Yang and Zhibing Li" 
        journal="J. Phys. Chem. C" 
        year="2018" 
        volume="122" 
        pages="27754" 
    >}}

## 2017 
- {{< article 
        title="Self-consistent DFT+U method for real-space time-dependent density functional theory calculations" 
        authors="Nicolas Tancogne-Dejean, Micael J. T. Oliveira, and Angel Rubio" 
        journal="Phys. Rev. B" 
        year="2017" 
        volume="96" 
        pages="245133" 
        doi="10.1103/PhysRevB.96.245133" 
    >}}
- {{< article 
        title="Ellipticity dependence of high-harmonic generation in solids originating from coupled intraband and interband dynamics" 
        authors="Nicolas Tancogne-Dejean, Oliver D. Mücke, Franz X. Kärtner, and Angel Rubio" 
        journal="Nat. Comm." 
        year="2017" 
        volume="8" 
        pages="745" 
        doi="10.1038/s41467-017-00764-5" 
    >}}
- {{< article 
        title="Impact of the Electronic Band Structure in High-Harmonic Generation Spectra of Solids" 
        authors="Nicolas Tancogne-Dejean, Oliver D. Mücke, Franz X. Kärtner, and Angel Rubio" 
        journal="Phys. Rev. Lett." 
        year="2017" 
        volume="118" 
        pages="087403" 
        doi="10.1103/PhysRevLett.118.087403" 
    >}}
- {{< article 
        title="Probing the π-π* transitions in conjugated compounds with an infrared femtosecond laser" 
        authors="Xi Liu, Pengcheng Li, Xiaosong Zhu, Pengfei Lan, Qingbin Zhang, and Peixiang Lu" 
        journal="Physical Review A" 
        year="2017" 
        volume="95" 
        pages="033421" 
        doi="10.1103/PhysRevA.95.033421" 
    >}}
- {{< article 
        title="High harmonic generation from axial chiral molecules" 
        authors="Dian Wang, Xiaosong Zhu, Xi Liu, Liang Li, Xiaofan, Zhang, Pengfei Lan, and Peixiang Lu" 
        journal="Optics Express" 
        year="2017" 
        volume="25" 
        pages="23502-23516" 
        doi="10.1364/OE.25.023502" 
    >}}

## 2016 
- {{< article 
        title="Laser-assisted field evaporation of metal oxides A time-dependent density functional theory study" 
        authors="Yu Xia and Zhibing Li" 
        journal="J. Chem. Phys." 
        year="2016" 
        volume="145" 
        pages="204704" 
        doi="10.1063/1.4968213" >}}
- {{< article 
        title="Selection rules of high-order-harmonic generation: Symmetries of molecules and laser fields" 
        authors="Xi Liu, Xiaosong Zhu, Liang Li, Yang Li, Qingbin Zhang, Pengfei Lan, and Peixiang Lu" 
        journal="Physical Review A" 
        year="2016" 
        volume="94" 
        pages="033410" 
        doi="10.1103/PhysRevA.94.033410" 
    >}}
- {{< article 
        title="Anomalous circular dichroism in high harmonic generation of stereoisomers with two chiral centers" 
        authors="Xiaosong Zhu, Xi Liu, Pengfei Lan, Dian Wang, Qingbin Zhang, Wei Li, and Peixiang Lu" 
        journal="Optics Express" 
        year="2016" 
        volume="24" 
        pages="24824-24835" 
        doi="10.1364/OE.24.024824" 
    >}}



## 2015 

- {{< article 
        title="Real-space grids and the Octopus code as tools for the development of new simulation approaches for electronic systems" 
        authors="Xavier Andrade, David A. Strubbe, Umberto De Giovannini, Ask Hjorth Larsen, Micael J. T. Oliveira, Joseba Alberdi-Rodriguez, Alejandro Varas, Iris Theophilou, Nicole Helbig, Matthieu Verstraete, Lorenzo Stella, Fernando Nogueira, Alán Aspuru-Guzik, Alberto Castro, Miguel A. L. Marques, Ángel Rubio" journal="Physical Chemistry Chemical Physics" 
        year="2015" 
        volume="17" 
        pages="31371-31396" 
        doi="10.1039/C5CP00351B " 
    >}}
- {{< article 
        title="Optical Spectra of the Special Au<sub>144</sub> Gold-Cluster Compounds: Sensitivity to Structure and Symmetry" 
        authors="H.-Ch. Weissker, O. Lopez-Acevedo, R. L. Whetten, and X. López-Lozano" 
        journal="J. Phys. Chem. C" 
        year="2015" 
        pages=""
        doi="10.1021/jp512310x"
    >}}
- {{< article 
        title="Ultrafast single electron spin manipulation in 2D semiconductor quantum dots with optimally controlled time-dependent electric fields through spin-orbit coupling" 
        authors="J. A. B. Marcilla, and A. Castro " 
        journal="The European Physical Journal B" 
        year="2015" 
        volume="88" 
        pages="15" 
        doi="10.1140/epjb/e2014-50700-5" 
    >}}
- {{< article 
        title="Computational benchmarking for ultrafast electron dynamics: wavefunction methods vs density functional theory" 
        authors="M. J. T. Oliveira, B. Mignolet, T. Kus, T. A. Papadopoulos, F. Remacle , and M. J. Verstraete" 
        journal="Journal of Chemical Theory and Computing" 
        year="2015" 
        volume=""
        pages="" 
        doi="10.1021/acs.jctc.5b00167" 
    >}}
- {{< article 
        title="Ab initio theory of spin entanglement in atoms and molecules" 
        authors="S. Pittalis, F. Troiani, C. A. Rozzi, and G. Vignale" 
        journal="Physical Review B" 
        year="2015" 
        volume="91" 
        pages="075109" 
        doi="10.1103/PhysRevB.91.075109" 
    >}}
- {{< article 
        title="Quantum-ionic features in the absorption spectra of homonuclear diatomic molecules" 
        authors="A. Crawford-Uranga, D. J. Mowbray, and D. M. Cardamone" 
        journal="Physical Review A" 
        year="2015" 
        volume="91" 
        pages="033410" 
        doi="10.1103/PhysRevA.91.033410" 
    >}}
- {{< article 
        title="Theoretical study of the channeling effect in the electronic stopping power of silicon carbide nanocrystal for low-energy protons and helium ions" authors="Fei Mao, Chao Zhang, Feng-Shou Zhang" 
        journal="Nuclear Instruments and Methods in Physics Research Section B: Beam Interactions with Materials and Atoms" 
        year="2015" 
        volume="342" 
        pages="215-220" 
        doi="10.1016/j.nimb.2014.09.035" 
    >}}
- {{< article 
        title="Exploring and Controlling Fragmentation of Polyatomic Molecules with Few-Cycle Laser Pulses" 
        authors="Markus Kitzler, Xinhua Xie, and Andrius Baltuška " 
        journal="Progress in Ultrafast Intense Laser Science XI" 
        year="2015" 
        volume="" 
        pages="43-72" 
        doi="10.1007/978-3-319-06731-5_3" 
    >}}
- {{< article 
        title="Configuration interaction singles based on the real-space numerical grid method: Kohn–Sham versus Hartree–Fock orbitals" 
        authors="Jaewook Kim, Kwangwoo Hong, Sunghwan Choi, Sang-Yeon Hwang and Woo Youn Kim " 
        journal="Physical Chemistry Chemical Physics" 
        year="2015" 
        volume="" 
        pages="" 
        doi="10.1039/C5CP00352K" 
    >}}
- {{< article 
        title="Surface passivation effects on the electronic and optical properties of 3C-SiC nanocrystals" 
        authors="Masoud Bezi Javan" 
        journal="Physica B: Condensed Matter" 
        year="2015" 
        volume="456" 
        pages="321–329" 
        doi="10.1016/j.physb.2014.09.019" 
    >}}
- {{< article 
        title="Charge density control of quantum dot lattices" 
        authors="Yousof Mardoukhi" 
        journal="Master's thesis" 
        year="2015" 
        volume="" 
        pages="" 
    >}}
- {{< article 
        title="High-order harmonic generation during propagation of femtosecond pulses through the laser-produced plasmas of semiconductors" 
        authors="R. A. Ganeev, M. Suzuki, S. Yoneya, and H. Kuroda " 
        journal="J. Appl. Phys." 
        year="2015" 
        volume="117" 
        pages="023114" 
        doi="10.1063/1.4905902" 
    >}}

## 2014 

- {{< article 
        title="Time-Dependent Density-Functional Theory of Strong-Field Ionization of Atoms under Soft X-Rays" 
        authors="A. Crawford-Uranga, U. De Giovannini, E. Räsaäen, M. J .T. Oliveira, D. J. Mowbray, G. M. Nikolopoulos, E. T. Karamatskos, D. Markellos, P. Lambropoulos, S. Kurth, A. Rubio" 
        journal="Phys. Rev. A" 
        volume="90" 
        pages="033412" 
        year="2014" 
        doi="10.1103/PhysRevA.90.033412" 
    >}}
- {{< article 
        title="Optical and Magnetic Excitations of Metal-Encapsulating Si Cages: A Systematic Study by Time-Dependent Density Functional Theory" 
        authors="M. J. T. Oliveira, P. V. C. Medeiros, J. R. F. Sousa, F. Nogueira, G. K. Gueorguiev" 
        journal="J. Phys. Chem. C" 
        volume="118" 
        pages="11377-11384" 
        year="2014" 
        doi="10.1021/jp4096562" 
    >}}
- {{< article 
        title="Real-time Simulations of Photoinduced Coherent Charge Transfer and Proton-Coupled Electron Transfer" 
        authors="T. J. Eisenmayer and F. Buda" journal="ChemPhysChem" 
        volume="15" 
        pages="3258–3263" 
        year="2014" 
        doi="10.1002/cphc.201402444" 
    >}}
- {{< article 
        title="Recent Memory and Performance Improvements in Octopus Code" 
        authors="J. Alberdi-Rodriguez, M. J. T. Oliveira, P. García-Risueño, Fernando Nogueira, J. Muguerza, A. Arruabarrena, A. Rubio" 
        journal="ICCSA 2014 - Lecture Notes in Computer Science" 
        volume="8582" 
        pages="607-622" 
        year="2014" 
        doi="10.1007/978-3-319-09147-1_44" 
    >}}
- {{< article 
        title="A survey of the parallel performance and accuracy of Poisson solvers for electronic structure calculations" 
        authors="P. García-Risueño, J. Alberdi-Rodriguez, M. J. T. Oliveira, X. Andrade, M. Pippig, J. Muguerza, A. Arruabarrena, A. Rubio" 
        journal="J. Comp. Chem" 
        volume="36" 
        pages="427-444" 
        year="2014" 
        doi="10.1002/jcc.23487" 
    >}}
- {{< article 
        title="Modelling the effect of nuclear motion on the attosecond time-resolved photoelectron spectra of ethylene" 
        authors="A Crawford-Uranga, U De Giovannini, D J Mowbray, S Kurth, A Rubio" 
        journal="J. Phys. B: At. Mol. Opt. Phys" 
        volume="47" 
        pages="124018" 
        year="2014" 
        doi="10.1088/0953-4075/47/12/124018" 
    >}}
- {{< article 
        title="TDDFT Study of the Optical Absorption Spectra of Bare Gold Clusters" 
        authors="Robertson W. Burgess and Vicki J. Keast" 
        journal="J. Phys. Chem. C" 
        year="2014" 
        volume="118" 
        pages="3194–3201" 
        doi="10.1021/jp408545c" 
    >}}

## 2013 
- {{< article 
        title="Real-Space Density Functional Theory on Graphical Processing Units: Computational Approach and Comparison to Gaussian Basis Set Methods" 
        authors="X. Andrade and A. Aspuru-Guzik" 
        journal="J. Chem. Theo. Comput." 
        volume="9 " 
        pages="4360-4373" 
        year="2013" 
        doi="10.1021/ct400520e" 
    >}}
- {{< article 
        title="Optimal local control of coherent dynamics in custom-made nanostructures" 
        authors="T. Blasi, M. F. Borunda, E. Rasanen, E. J. Heller" 
        journal="Phys. Rev. B" 
        volume="87" 
        pages="241303(R)" 
        year="2013" 
        doi="10.1103/PhysRevB.87.241303" 
    >}}
- {{< article 
        authors="C. A. Rozzi, S. M. Falke, N. Spallanzani, A. Rubio, E. Molinari, D. Brida, M. Maiuri, G. Cerullo, H. Schramm, J. Christoffers and C. Lienau" title="Quantum coherence controls the charge separation in a prototypical artificial light-harvesting system" 
        journal="Nat. Commun." 
        year="2013" 
        volume="4" 
        pages="1602" 
        doi="10.1038/ncomms2603" 
    >}}
- {{< article 
        authors="Y.L. Jiao, F. Wang, X.H. Hong, W.Y. Su, Q.H. Chen, and F.S. Zhang" 
        title="Electron dynamics in CaB6 induced by one- and two-color femtosecond laser" 
        journal="Phys. Lett. A" 
        volume="377" 
        pages="823-827" 
        year="2013" 
        doi="10.1016/j.physleta.2013.02.002" 
    >}}
- {{< article 
        authors="E. Räsänen and E. J. Heller " 
        title="Optimal control of quantum revival" 
        journal="Eur. Phys. J B" 
        year="2013" 
        volume="86" 
        pages="17" 
        doi="10.1140/epjb/e2012-30921-4" 
    >}}
- {{< article 
        title="Large two-dimensional electronic systems: Self-consistent energies and densities at low cost" 
        authors="E. Rasanen, S. Pittalis, S. and  G. Bekçioğlu, and I. Makkonen" 
        journal="Phys. Rev. B" 
        volume="87" 
        pages="035144" 
        year="2013" 
        doi="10.1103/PhysRevB.87.035144" 
    >}}
- {{< article 
        authors="L. Stella, P. Zhang, F. J. García-Vidal, A. Rubio, and P. García-González" 
        title="Performance of Nonlocal Optics When Applied to Plasmonic Nanostructures" 
        journal="J. Phys. Chem. C" 
        year="2013" 
        volume="117" 
        pages="8941–8949" 
        doi="10.1021/jp401887y" 
    >}}
- {{< article 
        authors="X. López Lozano, C. Mottet, and H.-Ch. Weissker" 
        journal="J. Phys. Chem. C" 
        title="Effect of Alloying on the Optical Properties of Ag–Au Nanoparticles" 
        year="2013" 
        pages="3062–3068" 
        volume="117" 
        doi="10.1021/jp309957y" 
    >}}
- {{< article 
        authors="J. I. Fuks, P. Elliott, A. Rubio, and N. T. Maitra" 
        title="Dynamics of Charge-Transfer Processes with Time-Dependent Density Functional Theory" 
        journal="J. Phys. Chem. Lett." 
        year="2013" 
        volume="4" 
        pages="735-739" 
        doi="10.1021/jz302099f" 
    >}}
- {{< article 
        title="Exchange-correlation asymptotics and high harmonic spectra" 
        authors="M. R. Mack, D. Whitenack, and A. Wasserman" 
        journal="Chemical Physics Letters" 
        volume="558" 
        year="2013" 
        pages="15-19" 
        doi="10.1016/j.cplett.2012.11.045" 
    >}}
- {{< article 
        title="Theoretical Shaping of Femtosecond Laser Pulses for Ultrafast Molecular Photo-Dissociation with Control Techniques Based on Time-Dependent Density Functional Theory" 
        authors="A. Castro" 
        journal="ChemPhysChem" 
        volume="14" 
        pages="1488-1495" 
        year="2013" 
        doi="10.1002/cphc.201201021" 
    >}}
- {{< article 
        title="Nonlinear ionization mechanism dependence of energy absorption in diamond under femtosecond laser irradiation" 
        authors="C. Wang, L. Jiang, X. Li, F. Wang, Y. Yuan, L. Qu, and Y. Lu" 
        journal="J. Appl. Phys." 
        volume="113" 
        pages="143106" 
        year="2013" 
        doi="10.1063/1.4801802" 
    >}}
- {{< article 
        title="Spin-orbit effects in the bismuth atom and dimer: tight-binding and density functional theory comparison " 
        authors="M.J.T. Oliveira, and X. Gonze" 
        journal="J. Phys. B: At. Mol. Opt. Phys." 
        volume="46" 
        pages="095101" 
        year="2013" 
        doi="10.1088/0953-4075/46/9/095101" 
    >}}
- {{< article 
        title="Simulating Pump-Probe Photoelectron and Absorption Spectroscopy on the Attosecond Timescale with Time-Dependent Density Functional Theory" 
        authors="U. De Giovannini, G. Brunetto,  A. Castro,  J. Walkenhorst and A. Rubio" 
        journal="Chemphyschem" 
        year="2013" 
        volume="14" 
        number="7" 
        pages="1363-1376" 
        doi="10.1002/cphc.201201007" 
    >}}
- {{< article 
        title="Stark Ionization of Atoms and Molecules within Density Functional Resonance Theory" 
        authors="A. H. Larsen, U. De Giovannini, D. L. Whitenack, A. Wasserman and A. Rubio" 
        journal="J. Phys. Chem. Lett." 
        year="2013" 
        pages="2734-2738" 
        doi="10.1021/jz401110h"
     >}}

## 2012 
- {{< article 
        title="Plasmon excitations in sodium atomic planes: A time-dependent density functional theory study" 
        authors="Bao-Ji Wang, Yuehua Xu and San-Huang Ke" 
        journal="J. Chem. Phys." 
        volume="137" 
        pages="054101" 
        year="2012" 
        doi="10.1063/1.4739952" 
    >}}
- {{< article 
        title="Optical control of entangled states in semiconductor quantum wells" 
        authors="E. Rasanen, T. Blasi, M. F. Borunda, E. J. Heller" 
        journal="Phys. Rev. B" 
        volume="86" 
        pages="205308" 
        year="2012" 
        doi="10.1103/PhysRevB.86.205308" 
    >}}
- {{< article 
        authors="X. Andrade, J. N. Sanders, and A. Aspuru-Guzik" 
        title="Application of compressed sensing to the simulation of atomic systems" 
        journal="Proc. Nat. Acad. Sci. USA" 
        volume="109" 
        pages="13928-13933" 
        year="2012" 
        doi="10.1073/pnas.1209890109" 
    >}}
- {{< article 
        authors="U. De Giovannini, D. Varsano, M. A. L. Marques, H. Appel, E. K. U. Gross, and A. Rubio" 
        title="Ab initio angle- and energy-resolved photoelectron spectroscopy with time-dependent density-functional theory" 
        journal="Physical Review A" 
        volume="85" 
        pages="062515" 
        year="2012" 
        doi="doi/10.1103/PhysRevA.85.062515" 
    >}}
- {{< article 
        authors="M. Wanko, P. García-Risueño, A. Rubio" 
        title="Excited states of the green fluorescent protein chromophore: Performance of ab initio and semi-empirical methods" 
        journal="physica status solidi (b)" 
        volume="249" 
        pages="392-400" 
        year="2012" 
        doi="10.1002/pssb.201100536" 
    >}}
- {{< article 
        authors="X. Andrade, J. Alberdi-Rodriguez, D. A. Strubbe, M. J. T. Oliveira, F. Nogueira, A. Castro, J. Muguerza, A. Arruabarrena, S. G. Louie, A. Aspuru-Guzik, A. Rubio, and M. A. L. Marques" 
        title="TDDFT in massively parallel computer architectures: the OCTOPUS project" 
        journal="J. Phys.: Condens. Matter" 
        volume="24" 
        pages="233202" 
        year="2012" 
        doi="10.1088/0953-8984/24/23/233202" 
    >}}
- {{< article 
        authors="J. G. Vilhena, E. Räsänen, L. Lehtovaara, and M. A. L. Marques " 
        title="Violation of a local form of the Lieb-Oxford bound" 
        journal="Physical Review A" 
        volume="85" 
        pages="052514" 
        year="2012" 
        doi="10.1103/PhysRevA.85.052514" 
    >}}
- {{< article 
        authors="J. Deslippe, G. Samsonidze, D. A. Strubbe, M. Jain, M. L. Cohen, and S. G. Louie" 
        title="BerkeleyGW: A Massively Parallel Computer Package for the Calculation of the Quasiparticle and Optical Properties of Materials and Nanostructures" 
        journal="Comput. Phys. Commun." 
        volume="183" 
        pages="1269" 
        year="2012" 
        doi="10.1016/j.cpc.2011.12.006" 
    >}}
- {{< article 
        authors="R.L. Aggarwal, L.W. Farrar, S.K. Saikin, X. Andrade, A. Aspuru-Guzik, D.L. Polla" 
        title="Measurement of the absolute Raman cross section of the optical phonons in type Ia natural diamond" 
        journal="Solid State Communications" 
        volume="152" 
        pages="204-209" 
        year="2012" 
        doi="10.1016/j.ssc.2011.11.005" 
    >}}
- {{< article 
        authors="X. Andrade and L. Genovese" 
        title="Harnessing the power of graphical processing units" 
        book="Fundamentals of time-dependent density functional theory (Springer Berlin / Heidelberg)" 
        pages="401­-413"
        year="2012"
    >}}
- {{< article 
        authors="A. Castro, M. Isla, José I. Martínez, and J.A. Alonso" 
        title="Scattering of a proton with the Li4 cluster: Non-adiabatic molecular dynamics description based on time-dependent density-functional theory" 
        journal="Chem. Phys." 
        year="2012" 
        pages="130-134" 
        volume="399" 
        doi="10.1016/j.chemphys.2011.07.005" 
    >}}
- {{< article 
        authors="P. Elliott, J. I. Fuks, A. Rubio, and N. T. Maitra" 
        journal="Phys. Rev. Lett." 
        volume="109" 
        title="Universal Dynamical Steps in the Exact Time-Dependent Exchange-Correlation Potential" 
        pages="266404" 
        year="2012" 
        doi="10.1103/PhysRevLett.109.266404" 
    >}}
- {{< article 
        authors="V. Kotimäki, E. Cicek, A. Siddiki, and E Räsänen" 
        title="Time-dependent transport in Aharonov–Bohm interferometers " 
        journal="New J. Phys." 
        volume="14" 
        pages="053024" 
        doi="10.1088/1367-2630/14/5/053024" 
        year="2012" 
    >}}
- {{< article 
        authors="S. Raghunathan and M. Nest" 
        title="The Lack of Resonance Problem in Coherent Control with Real-Time Time-Dependent Density Functional Theory" 
        journal="J. Chem. Theory Comput." 
        year="2012" 
        volume="8" 
        pages="806–809" 
        doi="10.1021/ct200905z"
    >}}
- {{< article 
        title="Theoretical Study of the Optical Absorption Spectra for Small-Size Silicon Clusters" 
        authors="J. Yalong, S. Nana, H. Xuhai, W. Feng" 
        year="2012" 
        journal="Sci. J. Phys. Sci." 
        volume="2" 
        pages="25-31" 
    >}}
- {{< article 
        title="Transient localized electron dynamics simulation during femtosecond laser tunnel ionization of diamond" 
        authors="C. Wang, L. Jiang, F. Wang, X. Li, Y.P. Yuan, L.T. Qu, and Y.F. Lu" 
        journal="Physics Letters A" 
        volume="376" 
        pages="3327-3331" 
        year="2012" 
        doi="10.1016/j.physleta.2012.07.039" 
    >}}
- {{< article 
        title="Limits of the Creation of Electronic Wave Packets Using Time-Dependent Density Functional Theory" 
        authors="S. Raghunathan and M. Nest" 
        journal="J. Phys. Chem. A" 
        year="2012" 
        volume="116" 
        pages="8490-8493" 
        doi="10.1021/jp3047483" 
    >}}
- {{< article 
        title="Characterization of TiO2 atomic crystals for nanocomposite materials oriented to optoelectronics" 
        authors="L. Chiodo, A. Massaro, S. Laricchia, F. Della Sala, R. Cingolani, M. Salazar, A. H. Romero, and A. Rubio " 
        journal="Optical and Quantum Electronics" 
        volume="44" 
        pages="291-296" 
        year="2012" 
        doi="10.1007/s11082-012-9559-y" 
    >}}
- {{< article 
        title="Time-dependent density functional theory study of charge transfer in collisions" 
        authors="G. Avendaño-Franco, B. Piraux, M. Grüning, and X. Gonze" 
        journal="Theoretical Chemistry Accounts" 
        year="2012" 
        volume="131" 
        pages="1289" 
        doi="10.1007/s00214-012-1289-5" 
    >}}
- {{< article 
        title="Ab-initio Calculation of the Ground State of a Truncated Single-Walled Carbon Nanotube" 
        authors="Md. M. Rahman and R. B. Ahmad" 
        journal="Journal of Applied Sciences Research" volume="8" pages="2575-2580" year="2012" >}}
- {{< article 
        title="A review of High Performance Computing foundations for scientists" 
        authors="P. García-Risueño and P.E. Ibáñez" 
        journal="Int. J. Mod. Phys. C" 
        volume="23" 
        pages="1230001" 
        doi="10.1142/S0129183112300011" 
        year="2012" 
    >}}

## 2011 
- {{< article 
        authors="X. Andrade and A. Aspuru-Guzik" 
        title="Prediction of the Derivative Discontinuity in Density Functional Theory from an Electrostatic Description of the Exchange and Correlation Potential" 
        journal="Physical Review Letters" 
        volume="107" 
        pages="183002" 
        year="2011" 
        doi="10.1103/PhysRevLett.107.183002" 
    >}}
- {{< article 
        authors="K. Shibata, K. Seki, P. J. J. Luukko, E. Räsänen K. M. Cha, I. Horiuchi, and K. Hirakawa" 
        title="Electronic structures in single self-assembled InAs quantum dashes detected by nanogap metal electrodes" 
        journal="Appl. Phys. Lett." 
        volume="99" 
        pages="182104" 
        year="2011" 
        doi="10.1063/1.3659479" 
    >}}
- {{< article 
        authors="J. Ojajärvi, E. Räsänen, S. Sadewasser, S. Lehmann, Ph. Wagner, and M. Ch. Lux-Steiner" 
        title="Tetrahedral Chalcopyrite Quantum Dots for Solar-cell Applications" 
        journal="Appl. Phys. Lett." 
        volume="99" 
        pages="111907" 
        year="2011" 
        doi="10.1063/1.3640225" 
    >}}
- {{< article 
        authors="Osman Bariş Malcıoğlu, Arrigo Calzolari, Ralph Gebauer, Daniele Varsano, and Stefano Baroni" 
        title="Dielectric and Thermal Effects on the Optical Properties of Natural Dyes: A Case Study on Solvated Cyanin" 
        journal="J. Am. Chem. Soc." 
        volume="133" 
        pages="15425" 
        year="2011" 
        doi="10.1021/ja201733v" 
    >}}
- {{< article 
        authors="Roberto Olivares-Amaya, Michael Stopa, Xavier Andrade, Mark A. Watson, and Alán Aspuru-Guzik" 
        title="Anion Stabilization in Electrostatic Environments" 
        journal="Journal of Physical Chemistry Letters" 
        volume="2" 
        pages="682-688" 
        year="2011" 
        doi="10.1021/jz200120w" 
    >}}
- {{< article 
        authors="F. Wang, X.C. Xu, X.H. Hong, J. Wang and B.C. Goua" 
        title="A theoretical model for electron transfer in ion–atom collisions: Calculations for the collision of a proton with an argon atom" 
        journal="Physics Letters A" 
        volume="375" 
        pages="3290-3295" 
        year="2011" 
        doi="10.1016/j.physleta.2011.07.032" 
    >}}
- {{< article 
        authors="M. Afshar, S. Sadewasser, J. Albert, S. Lehmann, D. Abou-Ras, D. Fuertes Marrón, A. A. Rockett, E. Räsänen and M. Ch. Lux-Steiner" 
        title="Chalcopyrite Semiconductors for Quantum Well Solar Cells" 
        journal="Advanced Energy Materials" 
        volume="1" 
        pages="1109" 
        year="2011" 
        doi="10.1002/aenm.201100362" 
    >}}
- {{< article 
        authors="Hans-Christian Weissker, Ning Ning, Friedhelm Bechstedt, and Holger Vach" 
        title="Luminescence and absorption in germanium and silicon nanocrystals: The influence of compression, surface reconstruction, optical excitation, and spin-orbit splitting" 
        journal="Phys. Rev. B" 
        volume="83" 
        pages="125413" 
        year="2011" 
        doi="10.1103/PhysRevB.83.125413" 
    >}}
- {{< article 
        authors="MB. S. Kirketerp, K. Kilsa, L.A. Espinosa Leal, D. Varsano, A. Rubio, M. B. Nielsen, S. B. Nielsen" 
        title="On the Intrinsic Optical Absorptions of Tetrathiafulvalene Radical Cations and Isomers" 
        journal="Chem. Comm." 
        volume="47" 
        pages="6900" 
        year="2011" 
        doi="10.1039/C1CC11936B" 
    >}}
- {{< article 
        authors="J. A. Bradley, A. Sakko, G. T. Seidler, A. Rubio, M. Hakala, K. Hämäläinen, G. Cooper, A. P. Hitchcock, K. Schlimmer, and K. P. Nagle" title="Reexamining the Lyman-Birge-Hopfield band of N2" 
        journal="Phys. Rev. A" 
        volume="84" 
        pages="022510" 
        year="2011" 
        doi="10.1103/PhysRevA.84.022510" 
    >}}
- {{< article 
        authors="G. P. Zhang, David A. Strubbe, Steven G. Louie, and Thomas F. George" 
        title="First-principles prediction of optical second-order harmonic generation in the endohedral N@C<sub>60</sub> compound" 
        journal="Phys. Rev. A" 
        volume="84" 
        pages="023837" 
        year="2011" 
        doi="10.1103/PhysRevA.84.023837" 
    >}}
- {{< article 
        authors="J. Thingna, R. Prasad, and S. Auluck" 
        title="Photo-absorption spectra of small hydrogenated silicon clusters using time-dependent density functional theory" 
        journal="Journal of Physics and Chemistry of Solids" 
        volume="72" 
        pages="1096-1100" 
        year="2011" 
        doi="10.1016/j.jpcs.2011.06.011" 
    >}}
- {{< article 
        authors="A. Bonaca and G. Bilalbegovic" 
        title="Electronic absorption spectra of hydrogenated protonated naphthalene and proflavine" 
        journal="Monthly Notices of the Royal Astronomical Society (MNRAS)" 
        volume="416" 
        pages="1509–1513" 
        year="2011" 
        doi="10.1111/j.1365-2966.2011.19149.x" 
    >}}
- {{< article 
        authors="S. Raghunathan and M. Nest" 
        title="Critical Examination of Explicitly Time-Dependent Density Functional Theory for Coherent Control of Dipole Switching" 
        journal="Journal of Chemical Theory and Computation" 
        volume="7" 
        pages="2492-2497" 
        year="2011" 
        doi="10.1021/ct200270t" 
    >}}
- {{< article 
        authors="N. Helbig, J. I. Fuks, M. Casula, M. J. Verstraete, M. A. L. Marques, I. V. Tokatly, and A. Rubio " 
        title="Density functional theory beyond the linear regime: Validating an adiabatic local density approximation" 
        journal="Physical Review A" 
        volume="8" 
        pages="032503" 
        year="2011" 
        doi="10.1103/PhysRevA.83.032503" 
    >}}
- {{< article 
        authors="M.J.T.Oliveira, S. Botti, and M.A.L. Marques " 
        title="Modeling van der Waals interactions between proteins and inorganic surfaces from time-dependent density functional theory calculations" 
        journal="Phys. Chem. Chem. Phys." 
        volume="13" 
        pages="15055-15061" 
        year="2011" 
        doi="10.1039/C1CP20719A" 
    >}}

## 2010 
- {{< article 
        authors="A. Sakko, A. Rubio, M. Hakala, and K. Hämäläinen" 
        title="Time-dependent density functional approach for the calculation of inelastic x-ray scattering spectra of molecules" 
        journal="J. Chem. Phys." 
        volume="133" 
        pages="174111" 
        year="2010" 
        doi="10.1063/1.3503594" 
    >}}
- {{< article 
        authors="Y. Suzuki, and K. Yamashita" 
        title="Real-time electron dynamics simulation of the adsorption of an oxygen molecule on Pt and Au clusters" 
        journal="Chemical Physics Letters" 
        volume="486" 
        pages="48" 
        year="2010" 
        doi="10.1016/j.cplett.2009.12.094" 
    >}}
- {{< article 
        authors="M. J. T. Oliveira, E. Räsänen, S. Pittalis, and M. A. L. Marques" 
        title="Toward an All-Around Semilocal Potential for Electronic Exchange" 
        journal="Journal of Chemical Theory and Computation" 
        volume="6" 
        pages="3664" 
        year="2010" 
        doi="10.1021/ct100448x" 
    >}}
- {{< article 
        title="Exchange-correlation potential with a proper long-range behavior for harmonically confined electron droplets" 
        authors="S. Pittalis and E. Räsänen" 
        journal="Phys. Rev. B" 
        volume="82" 
        pages="195124" 
        year="2010" 
        doi="10.1103/PhysRevB.82.195124" 
    >}}
- {{< article 
        title="Ultrafast sequential charge transfer in a double quantum dot" 
        authors="A. Putaja and E. Räsänen" 
        journal="Phys. Rev. B" 
        volume="82" 
        pages="165336" 
        year="2010" 
        doi="10.1103/PhysRevB.82.165336" 
    >}}
- {{< article 
        title="Laplacian-level density functionals for the exchange-correlation energy of low-dimensional nanostructures" 
        authors="S. Pittalis and E. Räsänen" 
        journal="Phys. Rev. B" 
        volume="82" 
        pages="165123" 
        year="2010" 
        doi="10.1103/PhysRevB.82.165123" 
    >}}
- {{< article 
        title="Interaction-Induced Spin Polarization in Quantum Dots" 
        authors="M. C. Rogge, E. Räsänen, and R. J. Haug " 
        journal="Phys. Rev. Lett." 
        volume="105" 
        pages="046802" 
        year="2010" 
        doi="10.1103/PhysRevLett.105.046802" 
    >}}
- {{< article 
        title="Colle-Salvetti-type local density functional for the exchange-correlation energy in two dimensions" 
        authors="S. Sakiroglu and E. Räsänen" 
        journal="Phys. Rev. A" 
        volume="82" 
        pages="012505" 
        year="2010" 
        doi="10.1103/PhysRevA.82.012505" 
    >}}
- {{< article 
        title="Aharonov-Bohm effect in many-electron quantum rings" 
        authors="V. Kotimäki and E. Räsänen" journal="Phys. Rev. B" 
        volume="81" 
        pages="245316" 
        year="2010" 
        doi="10.1103/PhysRevB.81.245316" 
    >}}
- {{< article 
        title="Parameter-free density functional for the correlation energy in two dimensions" 
        authors="E. Räsänen, S. Pittalis, and C. R. Proetto" 
        journal="Phys. Rev. B" 
        volume="81" 
        pages="195103" 
        year="2010" 
        doi="10.1103/PhysRevB.81.195103" 
    >}}
- {{< article 
        title="Becke-Johnson-type exchange potential for two-dimensional systems" 
        authors="S. Pittalis, E. Räsänen, and C. R. Proetto" 
        journal="Phys. Rev. B" 
        volume="81" 
        pages="115108" 
        year="2010" 
        doi="10.1103/PhysRevB.81.115108" 
    >}}
- {{< article 
        title="Semi-local density functional for the exchange-correlation energy of electrons in two dimensions" 
        authors="E. Räsänen, S. Pittalis, J. G. Vilhena, and M. Marques" 
        journal="Int. J. Quantum Chem." 
        volume="110" 
        pages="2308" 
        year="2010" 
        doi="10.1002/qua.22604" 
    >}}
- {{< article 
        title="Universal correction for the Becke–Johnson exchange potential" 
        authors="E. Räsänen, S. Pittalis, and C. R. Proetto" 
        journal="J. Chem. Phys." 
        volume="132" 
        pages="044112" 
        year="2010" 
        doi="10.1063/1.3300063" 
    >}}
- {{< article 
        title="On the lower bound on the exchange-correlation energy in two dimensions" 
        authors="E. Räsänen, S. Pittalis, C. R. Proetto, and K. Capelle" 
        journal="Physica E" 
        volume="42" 
        pages="1236" 
        year="2010" 
        doi="10.1016/j.physe.2009.11.126" 
    >}}
- {{< article 
        title="Exchange and correlation energy functionals for two-dimensional open-shell systems" 
        authors="E. Räsänen and S. Pittalis" 
        journal="Physica E" 
        volume="42" 
        pages="1232" 
        year="2010" 
        doi="10.1016/j.physe.2009.11.128" 
    >}}
- {{< article 
        title="Optical spectrum of proflavine and its ions" 
        authors="A. Bonaca and G. Bilalbegović" 
        journal="Chem. Phys. Lett." 
        volume="493" 
        pages="33" 
        year="2010" 
        doi="10.1016/j.cplett.2010.05.003" 
    >}}
- {{< article 
        title="Basis set effects on the hyperpolarizability of CHCl<sub>3</sub>: Gaussian-type orbitals, numerical basis sets and real-space grids" 
        authors="F. D. Vila, D. A. Strubbe, Y. Takimoto, X. Andrade, A. Rubio, S. G. Louie, and J. J. Rehr" 
        journal="J. Chem. Phys." 
        volume="133" 
        pages="034111" 
        year="2010" 
        doi="10.1063/1.3457362" 
    >}}

## 2009 
- {{< article 
        title="Stochastic quantum molecular dynamics" 
        authors="H. Appel and M. Di Ventra" 
        journal="Phys. Rev. B" 
        volume="80" 
        pages="212303" 
        year="2009" 
        url="http://prb.aps.org/abstract/PRB/v80/i21/e212303" 
        link="aps" 
    >}}
- {{< article 
        title="The challenge of predicting optical properties of biomolecules: What can we learn from time-dependent density-functional theory?" 
        authors="A. Castro, M. A. L. Marques, D. Varsano, F. Sottile, and A. Rubio" 
        journal="Compte Rendus Physique" 
        volume="10" 
        pages="469-490" 
        year="2009" 
        doi="10.1016/j.crhy.2008.09.001" 
    >}}
- {{< article 
        title="Lower Bounds on the Exchange-Correlation Energy in Reduced Dimensions" 
        authors="E. Räsänen, S. Pittalis, K. Capelle, and C. R. Proetto" 
        journal="Phys. Rev. Lett." 
        volume="102" 
        pages="206406" 
        year="2009" 
        doi="10.1103/PhysRevLett.102.206406" 
    >}}
- {{< article 
        title="Orbital-free energy functional for electrons in two dimensions" 
        authors="S. Pittalis and E. Räsänen" 
        journal="Phys. Rev. B" 
        volume="80" 
        pages="165112" 
        year="2009" 
        doi="10.1103/PhysRevB.80.165112" 
    >}}
- {{< article 
        title="Femtosecond laser pulse shaping for enhanced ionization" 
        authors="A. Castro, E. Räsänen, A. Rubio, and E. K. U. Gross" 
        journal="EPL" 
        volume="87" 
        pages="53001" 
        year="2009" 
        doi="10.1209/0295-5075/87/53001" 
    >}}
- {{< article 
        title="Gaussian approximations for the exchange-energy functional of current-carrying states: Applications to two-dimensional systems" 
        authors="S. Pittalis, E. Räsänen, and E. K. U. Gross" 
        journal="Phys. Rev. A" 
        volume="80" 
        pages="032515" 
        year="2009" 
        doi="10.1103/PhysRevA.80.032515"
    >}}
- {{< article 
        title="Optoelectronic Properties of Natural Cyanin Dyes" 
        authors="A. Calzolari, D. Varsano, A. Ruini, A. Catellani, R. Tel-Vered, H. B. Yildiz, O. Ovits and I. Willner" 
        journal="J. Phys. Chem. A" 
        volume="113" 
        pages="8801" 
        year="2009" 
        doi="10.1021/jp904966d" 
    >}}
- {{< article 
        title="Fingerprints of Bonding Motifs in DNA Duplexes of Adenine and Thymine Revealed from Circular Dichroism: Synchrotron Radiation Experiments and TDDFT Calculations" 
        authors="L. M. Nielsen, A. I. S. Holm, D. Varsano, U. Kadhane, S. V. Hoffmann, R. Di Felice, A. Rubio and S. B. Nielsen" 
        journal="J. Phys. Chem. B" 
        volume="113" 
        pages="9614" 
        year="2009" 
        doi="10.1021/jp9032029" 
    >}}
- {{< article 
        title="Exact Coulomb cutoff technique for supercell calculations in two dimensions" 
        authors="Alberto Castro, Esa Räsänen, and Carlo Rozzi" 
        journal="Phys. Rev. B" 
        volume="80" 
        pages="033102" 
        year="2009" 
        doi="10.1103/PhysRevB.80.033102" 
    >}}
- {{< article 
        title="Photo-excitation of a light-harvesting supra-molecular triad: a Time-Dependent DFT study " 
        authors="N. Spallanzani, C. A. Rozzi, D. Varsano, T. Baruah, M. R. Pederson,F. Manghi, and A. Rubio " 
        journal="J. Phys. Chem. B" 
        volume="113" 
        pages="5345" 
        year="2009" 
        doi="10.1021/jp900820q" 
    >}}
- {{< article 
        title="Towards a gauge invariant method for molecular chiroptical properties in TDDFT " 
        authors="D.Varsano, L.A. Espinosa-Leal, X. Andrade, M. A. L. Marques, R. di Felice and A. Rubio" 
        journal="Phys. Chem. Chem. Phys." 
        volume="11" 
        pages="4481" 
        year="2009" 
        doi="10.1039/b903200b" 
    >}}
- {{< article 
        title="Optical and magnetic properties of boron fullerenes" 
        authors="Silvana Botti, Alberto Castro, Nektarios N. Lathiotakis, Xavier Andrade and Miguel A. L. Marques" 
        journal="Phys. Chem. Chem. Phys." 
        volume="11" 
        pages="4523-4527" 
        year="2009" 
        doi="10.1039/b902278c" 
    >}}
- {{< article 
        title="Acceleration of quantum optimal control theory algorithms with mixing strategies" 
        authors="Alberto Castro and E. K. U. Gross" 
        journal="Phys. Rev. E" 
        volume="79" 
        pages="056704" 
        year="2009" 
        doi="10.1103/PhysRevE.79.056704" 
    >}}
- {{< article 
        title="Structure Dependence of Hyperpolarizability in Octopolar Molecules" 
        authors="C. Cardoso, P. E. Abreu and F. Nogueira" 
        journal="Journal of Chemical Theory and Computation" 
        volume="5" 
        pages="850-858" 
        year="2009" 
        doi="10.1021/ct800464t" 
    >}}
- {{< article 
        title="Revisiting Molecular Dissociation in Density Functional Theory: A Simple Model" 
        authors="David G. Tempel, Todd J. Martinez, and Neepa T. Maitra" 
        journal="Journal of Chemical Theory and Computation" 
        volume="5" 
        pages="770-780" 
        year="2009" 
        doi="10.1021/ct800535c" 
    >}}
- {{< article 
        title="First-principles theoretical spectroscopy with time-dependent density functional theory" 
        authors="J.I. Martinez, and E.M. Fernandez" 
        journal="The European Physical Journal D" 
        volume="52" 
        pages="199-202" 
        year="2009" 
        doi="10.1140/epjd/e2008-00244-6" 
    >}}
- {{< article 
        title="Modified Ehrenfest Formalism for Efficient Large-Scale ab initio Molecular Dynamics"    
        authors="Xavier Andrade, Alberto Castro, David Zueco, J. L. Alonso, Pablo Echenique, Fernando Falceto and Angel Rubio" 
        journal="Journal of Chemical Theory and Computation" 
        volume="5" 
        pages="728-742" 
        year="2009" 
        doi="10.1021/ct800518j" 
    >}}
- {{< article 
        title="Electronic exchange in quantum rings: Beyond the local-density approximation" 
        authors="E. Räsänen, S. Pittalis, C. R. Proetto, and E. K. U. Gross" 
        journal="Phys. Rev. B" 
        volume="79" 
        pages="121305(R)" 
        year="2009" 
        doi="10.1103/PhysRevB.79.121305" 
    >}}
- {{< article 
        title="Correlation energy of finite two-dimensional systems: Toward nonempirical and universal modeling" 
        authors="S. Pittalis, E. Räsänen, C. R. Proetto, and E. K. U. Gross" 
        journal="Phys. Rev. B" 
        volume="79" 
        pages="085316" 
        year="2009" 
        url="http://link.aps.org/abstract/PRB/v79/e085316" 
        link="aps" 
    >}}
- {{< article 
        title="Density gradients for the exchange energy of electrons in two dimensions" 
        authors="S. Pittalis, E. Räsänen, J. G. Vilhena, and M. A. L. Marques" 
        journal="Phys. Rev. A" 
        volume="79" 
        pages="012503" 
        year="2009" 
        doi="10.1103/PhysRevA.79.012503" 
    >}}
- {{< article 
        title="Photoabsorption spectra of small cationic xenon clusters from time-dependent density functional theory" 
        authors="M.J.T. Oliveira, F. Nogueira, M.A.L. Marques, and A. Rubio" 
        journal="J. Chem. Phys." 
        volume="131" 
        pages="214302" 
        year="2009" 
        doi="10.1063/1.3265767" 
    >}}

## 2008 

- {{< article 
        title="Plasmon resonances in linear atomic chains: Free-electron behavior and anisotropic screening of ''d'' electrons" 
        authors="Jun Yan and Shiwu Gao" 
        journal="Phys. Rev. B" 
        volume="78" 
        pages="235413" 
        year="2008" 
        doi="10.1103/PhysRevB.78.235413" 
    >}}
- {{< article 
        title="Local correlation functional for electrons in two dimensions" 
        authors="S. Pittalis, E. Räsänen, and M. A. L. Marques" 
        journal="Phys. Rev. B" 
        volume="78" 
        pages="195322" 
        year="2008" 
        doi="10.1103/PhysRevB.78.195322" 
    >}}
- {{< article 
        title="Dehydrogenated polycyclic aromatic hydrocarbons and UV bump" 
        authors="G. Malloci, G. Mula, C. Cecchi-Pestellini, and C. Joblin" 
        journal="Astronomy & Astrophysics" 
        volume="489" 
        pages="1183-1187" 
        year="2008" 
        doi="10.1051/0004-6361:200810177" 
    >}}
- {{< article 
        title="Efficient formalism for large-scale ab initio molecular dynamics based on time-dependent density functional theory " 
        authors="J. L. Alonso, X. Andrade, P. Echenique, F. Falceto, D. Prada-Gracia, and A. Rubio" 
        journal="Phys. Rev. Lett." 
        volume="101" 
        pages="096403" 
        year="2008" 
        doi="10.1103/PhysRevLett.101.096403" 
    >}}
- {{< article 
        title="Delta self-consistent field method to obtain potential energy surfaces of excited molecules on surfaces" 
        authors="J. Gavnholt, T. Olsen, M. Engelund, and J. Schiøtz" 
        journal="Phys. Rev. B" volume="78" 
        pages="075441" 
        year="2008" 
        doi="10.1103/PhysRevB.78.075441" 
    >}}
- {{< article 
        title="Carbonyl sulphide under strong laser field: Time-dependent density functional theory" 
        authors="G. Bilalbegović" 
        journal="Eur. Phys. J. D" 
        volume="49" 
        pages="43-49" 
        year="2008" 
        doi="10.1140/epjd/e2008-00137-8" 
    >}}
- {{< article 
        title="The role of dimensionality on the quenching of spin-orbit effects in the optics of gold nanostructures" 
        authors="Alberto Castro, Miguel A. L. Marques, Aldo H. Romero, Micael J. T. Oliveira, and Angel Rubio" 
        journal="J. Chem. Phys." 
        volume="129" 
        pages="144110" 
        year="2008" 
        doi="10.1063/1.2990745" 
    >}}
- {{< article 
        title="Experimental and ab-initio studies of the spectroscopic properties of N,N′,N″-triphenylguanidine and N,N′,N″-triphenylguanidinium chloride" 
        authors="C. Cardoso, P.S. Pereira Silva, M. Ramos Silva, A. Matos Beja, J.A. Paixao, F. Nogueira and A.J.F.N. Sobral" 
        journal="Journal of Molecular Structure" 
        volume="878" 
        pages="169" 
        year="2008" 
        doi="10.1016/j.molstruc.2007.08.012" 
    >}}
- {{< article 
        title="Electronic and atomic structure of the AlnHn+2 clusters" 
        authors="J. I. Martinez and J. A. Alonso" 
        journal="J. Chem. Phys." 
        volume="129" 
        pages="074306" 
        year="2008" 
        doi="10.1063/1.2960627" 
    >}}
- {{< article 
        title="Investigating interaction-induced chaos using time-dependent density-functional theory" 
        authors="A. Wasserman, N. T. Maitra, and E. J. Heller" 
        journal="Phys. Rev. A" 
        volume="77" 
        pages="042503" 
        year="2008" 
        doi="10.1103/PhysRevA.77.042503" 
    >}}
- {{< article 
        title="Photoabsorption in sodium clusters on the basis of time-dependent density-functional theory" 
        authors="J.-O. Joswig, L. O. Tunturivuori, and R. M. Nieminen" 
        journal="J. Chem. Phys." 
        volume="128" 
        pages="014707" 
        year="2008" 
        doi="10.1063/1.2814161" 
    >}}
- {{< article 
        title="Cluster-surface and cluster-cluster interactions: Ab initio calculations and modeling of asymptotic van der Waals forces" 
        authors="Silvana Botti, Alberto Castro, Xavier Andrade, Angel Rubio, and Miguel A. L. Marques" 
        journal="Phys. Rev. B" 
        volume="78" 
        pages="035333" 
        year="2008" 
        doi="10.1103/PhysRevB.78.035333" 
    >}}
- {{< article 
        title="On the Use of Neumann’s Principle for the Calculation of the Polarizability Tensor of Nanostructures " 
        authors="Micael J. T. Oliveira, Alberto Castro, Miguel A. L. Marques, and Angel Rubio " 
        journal="J. Nanoscience and Nanotechnology" 
        volume="8" 
        pages="3392" 
        year="2008" 
        doi="10.1166/jnn.2008.142" 
        url="http://nano-bio.ehu.es/use-neumanns-principle-calculation-polarizability-tensor-nanostructures"
    >}}
- {{< article 
        title="Density functional study of the structural and electronic properties of aluminium-lithium clusters " 
        authors="J. I Martinez, J. A. Alonso and A. Castro" 
        journal="J. Comp. Meth. Sci. Engineering" 
        volume="7" 
        pages="355" 
        year="2008" 
        url="http://iospress.metapress.com/content/th8287j6p73743w5/?p=fa0d10eaf32a406293e8fed9a2013e90&pi=2" 
    >}}
- {{< article 
        title="Electron localization function for two-dimensional systems" 
        authors="E. Räsänen, A. Castro and E. K. U. Gross" 
        journal="Phys. Rev. B" 
        volume="77" 
        pages="115108" 
        year="2008" 
        doi="10.1103/PhysRevB.77.115108" 
    >}}
- {{< article 
        title="Optimal laser control of double quantum dots" 
        authors="E. Räsänen, A. Castro, J. Werschnik, A. Rubio and E. K. U. Gross" 
        journal="Phys. Rev. B" 
        volume="77" 
        pages="085324" 
        year="2008" 
        doi="10.1103/PhysRevB.77.085324" 
    >}}
- {{< article 
        title="Coherent quantum switch driven by optimized laser pulses" 
        authors="E. Räsänen, A. Castro, J. Werschnik, A. Rubio and E. K. U. Gross" 
        journal="Physica E" 
        volume="40" 
        pages="1593" 
        year="2008" 
        doi="10.1016/j.physe.2007.09.181" 
    >}}
- {{< article 
        title="Sparse Non-Blocking Collectives in Quantum Mechanical Calculations" 
        authors="T. Hoefler, F. Lorenzen, A. Lumsdaine" 
        journal="To be presented at the EuroPVM/MPI Users Group Meeting" 
        year="2008" 
    >}}

## 2007 

- {{< article 
        title="End and Central Plasmon Resonances in Linear Atomic Chains" 
        authors="Jun Yan, Zhe Yuan, and Shiwu Gao" 
        journal="Phys. Rev. Lett." 
        volume="98" 
        pages="216602" 
        year="2007" 
        url="10.1103/PhysRevLett.98.216602" 
        link="arXiv" 
    >}}
- {{< article 
        title="Optimal Control of Quantum Rings by Terahertz Laser Pulses" 
        authors="E. Räsänen, A. Castro, J. Werschnik, A. Rubio, and E. K. U. Gross" 
        journal="Phys. Rev. Lett." 
        volume="98" 
        pages="157404 -4" 
        year="2007" 
        url="http://arxiv.org/abs/cond-mat/0611634" 
        link="arXiv" 
    >}}
- {{< article 
        title="Efficient calculation of van der Waals dispersion coefficients with time-dependent density functional theory in real time: Application to polycyclic aromatic hydrocarbons" 
        authors="Miguel A. L. Marques, Alberto Castro, and Silvana Botti" 
        journal="J. Chem. Phys." 
        volume="127" 
        pages="014107" 
        year="2007" 
        url="http://link.aip.org/link/JCPSA6/v127/i1/p014107/s1" 
        link="aip" 
    >}}
- {{< article 
        title="Time-dependent density functional theory scheme for efficient calculations of dynamic (hyper)polarizabilities," 
        authors="Xavier Andrade, Silvana Botti, Miguel Marques and Angel Rubio" 
        journal="J. Chem. Phys" 
        volume="126" 
        pages="184106" 
        year="2007" 
        url="http://scitation.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000126000018184106000001&idtype=cvips&gifs=Yes" 
        link="AIP" 
    >}}
- {{< article 
        title="Photoabsorption spectra of boron nitride fullerenelike structures" 
        authors="Laura Koponen, Lasse Tunturivuori, Martti J. Puska, and Risto M. Nieminen " 
        journal="J. Chem. Phys." 
        volume="126" 
        pages="214306" 
        year="2007" 
        url="http://scitation.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000126000021214306000001&idtype=cvips&gifs=yes" 
        link="aip" 
    >}}
- {{< article 
        title="Exchange-energy functionals for finite two-dimensional systems" 
        authors="S. Pittalis, E. Rasanen, N. Helbig, and E. K. U. Gross" 
        journal="Phys. Rev. B" 
        volume="76" pages="235314" 
        year="2007" 
        url="http://link.aps.org/abstract/PRB/v76/e235314" 
        link="aps" 
    >}}
- {{< article 
        title="Identification of CdSe fullerene-like nanoparticles from optical spectroscopy calculations" 
        authors="S. Botti and M.A.L. Marques" 
        journal="Phys. Rev. B" 
        volume="75" 
        pages="035311" 
        year="2007" 
        url="http://hdl.handle.net/10.1103/PhysRevB.75.035311" 
        link="hdl.handle.net" 
    >}}
- {{< article 
        title="Ab initio optical absorption spectra of size-expanded xDNA base assemblies" 
        authors="D. Varsano, A. Garbesi and R. Di Felice" 
        journal="J. Phys. Chem. B" 
        volume="111" 
        pages="14012" 
        year="2007" 
        doi="10.1021/jp075711z" 
        link="ACS" 
    >}}
- {{< article 
        title="Optical Absorption of a Green Fluorescent Protein variant: Environment Effects in a Density Functional Study" 
        authors="Carlo Camilloni, Davide Provasi, Guido Tiana, and Ricardo A. Broglia " 
        journal="J. Phys. Chem. B" 
        volume="111" 
        pages="10807 -10812" 
        year="2007" 
        doi="10.1021/jp072511e" 
        link="acs" 
    >}}
- {{< article 
        title="Theoretical study of molecular hydrogen clusters. Growth models and magic numbers" 
        authors="J.I. Martinez, M. Isla and J. A. Alonso" 
        journal="Eur. Phys. J. D" 
        volume="43" 
        pages="61-64" 
        year="2007" 
        url="http://www.edpsciences.org/articles/epjd/abs/2007/07/d06553/d06553.html" 
        link="EDPSCIENCES" 
    >}}
- {{< article 
        title="Massively-parallel eigensolver for the {{< octopus> >}} code" 
        authors="F. Lorenzen" 
        journal="Supercomputing in Europe &ndash; Report 2007"
        year="2007" 
        url="http://www.physik.fu-berlin.de/~lorenzen/physics/lorenzen2008lobpcg.pdf"
    >}}

## 2006 

- {{< article 
        title="Effect of different laser polarization direction on high order harmonic generation of N2 and H2" 
        authors="Lei Cui, J. Zhao, Y. J. Hu, Y. Y. Teng, X. H. Zeng, and B. Gu" 
        journal="Appl. Phys. Lett." 
        volume="89" 
        pages="211103" 
        year="2006" 
        url="http://scitation.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=APPLAB000089000021211103000001&idtype=cvips&gifs=yes" 
        link="aip" 
    >}}
- {{< article 
        title="Optical absorption spectra of $V_{4}^+$ Isomers: One example of first-principles theoretical spectrocopy with time-dependent density-functional theory" authors="J. I. Martinez, A. Castro, A. Rubio and J. A. Alonso," 
        journal="J. Comp. Theor. Nanoscience" 
        volume="3," 
        pages="761," 
        year="2006" 
        url="http://www.ingentaconnect.com/content/asp/jctn/2006/00000003/00000005/art00014token=004a1b0ba9470c146c6266d652f595c5f3b3b476728483f2573452a7b2f2a38ccf82cabfc7" 
        link="ingentaconnect" 
    >}}
- {{< article
        title="octopus: a tool for the application of time-dependent density functional theory" 
        journal="Scientific Highlight of the Month, PsiK Newsletter (Issue 73, February 2006)"
        url="http://psi-k.dl.ac.uk/newsletters/News_73/newsletter_73.pdf" 
        link="PsiK"
    >}}
- {{< article 
        title="octopus: a tool for the application of time-dependent density functional theory" 
        authors="A. Castro, H. Appel, Micael Oliveira, C.A. Rozzi, X. Andrade, F. Lorenzen, M.A.L. Marques, E.K.U. Gross, and A. Rubio" 
        journal="Phys. Stat. Sol. B" 
        volume="243" 
        pages="2465-2488" 
        year="2006" 
        url="http://hdl.handle.net/10.1002/pssb.200642067" 
        link="PSSB" 
    >}}
- {{< article 
        title="A TDDFT study of the excited states of DNA bases and their assemblies" 
        authors="D. Varsano, R. Di Felice, M.A.L. Marques and A. Rubio" 
        journal="Journal of Physical Chemistry B" 
        volume="110" 
        pages="7129 -7138" 
        year="2006" 
        doi="10.1021/jp056120g" 
    >}}
- {{< article 
        title="An exact Coulomb cutoff technique for supercell calculations" 
        authors="C.A. Rozzi, D. Varsano, A. Marini, E. K. U. Gross and A. Rubio" 
        journal="Physical Review B" 
        volume="73" 
        pages="205119" 
        year="2006" 
        doi="10.1103/PhysRevB.73.205119" 
    >}}

## 2005 

- {{< article 
        title="Calculation of vibrational frequencies within the real space pseudopotential approach" 
        authors="Eugene S. Kadantsev and M. J. Stott" 
        journal="Phys. Rev. B" 
        volume="71" 
        pages="045104" 
        year="2005" 
        doi="10.1103/PhysRevB.71.045104" 
        url="http://link.aps.org/doi/10.1103/PhysRevB.71.045104" 
    >}}
- {{< article
        title="Optical Absorption of the Blue Fluorescent Protein: a First Principles Study"
        authors="X. Lopez, M.A.L. Marques, A. Castro, and A. Rubio" 
        journal="J. Am. Chem. Soc." 
        volume="127" 
        pages="12329-12337"
        year="2005" 
        doi="10.1021/ja050935l"
    >}}
- {{< article
        title="Time-dependent electron localization function"
        authors="T. Burnus, M.A.L. Marques, and E.K.U. Gross"
        journal="Phys. Rev. A"
        volume="71" 
        pages="10501(R)"
        year="2005"
        url="http://link.aps.org/abstract/PRA/v71/e010501" 
        link="APS"
    >}}
- {{< article
        title="The planar-to-tubular structural transition in boron clusters from optical absorption"
        authors="M.A.L. Marques and Silvana Botti"
        journal="J. Chem. Phys."
        volume="123"
        pages="014310" 
        year="2005"
        url="http://scitation.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000123000001014310000001&idtype=cvips&gifs=Yes"
        link="AIP"
    >}}
- {{< article 
        title="Determination of the lowest-energy structure of Ag<sub>8</sub> from first-principles calculations" 
        authors="M. Pereiro and D. Baldomir"
        journal="Phys. Rev. A"
        volume="72"
        pages="045201" 
        year="2005" 
        url="http://link.aps.org/abstract/PRA/v72/e045201"
        link="APS"
    >}}
- {{< article
        title="Fragmentation and Coulomb explosion of deuterium clusters by the interaction with intense laser pulses"
        authors=" M. Isla, J.A. Alonso" 
        journal="Phys. Rev. A" 
        volume="72" 
        pages="023201" 
        year="2005" 
        url="http://link.aps.org/abstract/PRA/v72/e023201" 
        link="APS"
    >}}
- {{< article
        title="Theoretical electron affinities of PAHs and electronic absorption spectra of their mono-anions" 
        authors="Malloci G, Mulas G, Cappellini G, et al" 
        journal="Astronomy & Astrophysics"
        volume="432" 
        pages="585-594" 
        year="2005"
    >}}

## 2004 

- {{< article 
        title="Calculation of the optical spectrum of the Ti<sub>8</sub>C<sub>12</sub> and V<sub>8</sub>C<sub>12</sub> Met-Cars" 
        authors="Martinez JI, Castro A, Rubio A, et al." url="http://www.sciencedirect.com/science/article/pii/S0009261404014459" 
        journal="Chem. Phys. Lett." 
        volume="398"    pages="292-296" 
        year="2004" 
    >}}
- {{< article 
        title="Propagators for the time-dependent Kohn-Sham equations" 
        authors="A. Castro, M.A.L. Marques, and A. Rubio" 
        journal="J. Chem. Phys" 
        volume="121" 
        pages="3425-3433" 
        year="2004" 
        doi="10.1063/1.1774980" 
        url="http://link.aip.org/link/?JCPSA6/121/3425/1" 
        link="AIP"
    >}} 
- {{< article 
        title="Optical properties of nanostructures from time-dependent density functional theory" 
        authors="Alberto Castro, M.A.L. Marques, Julio A. Alonso, Angel Rubio" 
        journal="J. Comp. Theoret. Nanoscience" 
        volume='1' 
        pages="231-255" 
        year="2004" 
        url="http://www.aspbs.com/html/a1304jct.htm" 
        link="ASPBS" 
    >}}
- {{< article 
        title="Excited states dynamics in time-dependent density functional theory: high-field molecular dissociation and harmonic generation" 
        authors="Alberto Castro, M.A.L. Marques, Julio A. Alonso, George F. Bertsch, and Angel Rubio" 
        journal="Eur. Phys. J. D" 
        volume="28" 
        pages="211-218" 
        year="2004" 
        url="http://www.edpsciences.org/articles/epjd/abs/2004/02/d03019/d03019.html" 
        link="EDPS"
    >}}
- {{< article 
        title="Quasiparticle effects and optical absorption in small fullerenelike GaP clusters"
        authors="G. Malloci, G. Cappellini, G. Mulas, et al."
        journal="Phys. Rev. B" 
        volume="70" pages="205429" 
        year="2004" 
    >}}
- {{< article
        title="Electronic absorption spectra of PAHs up to vacuum UV - Towards a detailed model of interstellar PAH photophysics"
        authors="G. Malloci, G. Mulas, C. Joblin"
        journal="Astronomy & Astrophysics"
        volume="426" 
        pages="105-117" 
        year="2004"
    >}}
- {{< article
        title="Time and energy-resolved two photon photoemission of the Cu(100) and Cu(111) metal surfaces" 
        authors="D. Varsano, M.A.L. Marques, A. Rubio" 
        journal="Comput. Mat. Science"
        volume="30"
        pages="110-115"
        year="2004"
    >}}
- {{< article
        title="Laser-induced control of (multichannel) intracluster reactions - The slowest is always the easiest to take"
        authors="A.G. Urena, K. Gasmi, S. Skowronek, et al." 
        journal="European Phys. J. D"
        volume="28" 
        pages="193-198"
        year="2004"
    >}}

## 2003 
- {{< article
        title="Time-dependent density-functional approach for biological chromophores: the case of the green fluorescent protein"
        authors="M.A.L. Marques, Xabier López, Daniele Varsano, Alberto Castro, and Angel Rubio"
        journal="Phys. Rev. Lett."
        volume="90"
        pages="258101"
        year="2003" 
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=PRLTAO000090000025258101000001&idtype=cvips&gifs=yes&jsessionid=1669401059408246061"
        link="APS"
    >}}
- {{< article
        title="octopus: a first-principles tool for excited electron-ion dynamics"
        authors="M.A.L. Marques, Alberto Castro, George F. Bertsch, Angel Rubio" 
        journal="Comput. Phys. Commun."
        volume="151" 
        pages="60-78"
        year="2003"
        url="http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B6TJ5-473HYF7-3&_user=10&_handle=W-WA-A-A-AU-MsSAYVW-UUA-AUVZEBEVBU-WWEDBZBZY-AU-U&_fmt=summary&_coverDate=03%2F01%2F2003&_rdoc=6&_orig=browse&_srch=%23toc%235301%232003%23998489998%23376961!&_cdi=5301&view=c&_acct=C000050221&_version=1&_urlVersion=0&_userid=10&md5=350857099d3928104f3b1025ba144955"
        link="Sciencedirect"
    >}}

## 2002 

- {{< article 
        title="Can optical spectroscopy directly elucidate the ground state of C<sub>20</sub>?" 
        authors="Alberto Castro, M.A.L. Marques, Julio A. Alonso, George F. Bertsch, K. Yabana, and Angel Rubio" 
        journal="J. Chem. Phys."
        volume="116"
        pages="1930-1933"
        year="2002"
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000116000005001930000001&idtype=cvips&gifs=Yes"
        link="AIP" 
    >}}

## 2001 

- {{< article
        title="Assessment of exchange-correlation functionals for the calculation of dynamical properties of small clusters in time-dependent density functional theory"
        authors="M.A.L. Marques, Alberto Castro, and Angel Rubio"
        journal="J. Chem. Phys." 
        volume="115"
        pages="3006-3014"
        year="2001"
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000115000007003006000001&idtype=cvips&gifs=yes"
        link="AIP"
    >}}

##  With the old code  

- {{< article
        title="Optical response of small carbon clusters"
        authors="K. Yabana and G.F. Bertsch"
        journal="Z. Phys. D (Atom. Mol. Cl.)"
        volume="42" 
        pages="219-225"
        year="1997"
    >}}
- {{< article
        title="Optical response of small silver clusters"
        authors="K. Yabana, and G.F. Bertsch" 
        journal="Phys. Rev. A"
        volume="60"
        pages="3809-3814" 
        year="1999"
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=PLRAAN000060000005003809000001&idtype=cvips&gifs=yes"
        link="AIP"
    >}}
- {{< article
        title="Application of the time-dependent local density approximation to optical activity"
        authors="K. Yabana and G.F. Bertsch" 
        journal="Phys. Rev. A"
        volume="60"
        pages="1271-1279"
        year="1999"
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=PLRAAN000060000002001271000001&idtype=cvips&gifs=yes&jsessionid=2664771015890232713"
        link="AIP"
    >}} 
- {{< article
        title="Oscillator strengths with pseudopotentials" 
        authors="K. Yabana and G.F. Bertsch" 
        journal="Phys. Rev. A"
        volume="58" 
        pages="2604-2607"
        year="1999" 
        url="http://cornell.mirror.aps.org/abstract/PRA/v58/i3/p2604_1"
        link="APS"
    >}}
- {{< article
        title="Real-space computation of dynamic hyperpolarizabilities"
        authors="J.I. Iwata., K. Yabana, and G.F. Bertsch"
        journal="J. Chem. Phys."
        volume="115"
        pages="8773-8783"
        year="2001"
        url="http://link.aip.org/link/JCPSA6/v115/i19/p8773/s1" 
        link="AIP"
    >}}
- {{< article
        title="Electron-vibration coupling in time-dependent density-functional theory: Application to benzene" 
        authors="G.F. Bertsch, A. Schnell, and K. Yabana"
        journal="J. Chem. Phys."
        volume="115"
        pages="4051-4054"
        year="2001"
        url="http://ojps.aip.org/getabs/servlet/GetabsServlet?prog=normal&id=JCPSA6000115000009004051000001&idtype=cvips&gifs=Yes" 
        link="AIP"
    >}}
- {{< article
        title="Comparison of direct and Fourier space techniques in time-dependent density functional theory"
        authors="G.F. Bertsch, A. Rubio, and K. Yabana" 
        journal="archiv:physics/0003090" 
        url="http://xxx.unizar.es/abs/physics/0003090" 
    >}}
